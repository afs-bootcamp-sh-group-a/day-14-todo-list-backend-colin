package oocl.afs.todolist.advice;

public class NotFoundException extends RuntimeException{

    public static final String error = "出错";

    public NotFoundException() {
        super(error);
    }
}
