package oocl.afs.todolist.controller;

import oocl.afs.todolist.advice.ErrorResponse;
import oocl.afs.todolist.entity.Todo;
import oocl.afs.todolist.service.TodoService;
import oocl.afs.todolist.service.dto.TodoCreateRequest;
import oocl.afs.todolist.service.dto.TodoResponse;
import oocl.afs.todolist.service.mapper.TodoMapper;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/todo")
public class TodoController {
    private final TodoService todoService;
    private final TodoMapper todoMapper;

    public TodoController(TodoService todoService, TodoMapper todoMapper) {
        this.todoService = todoService;
        this.todoMapper = todoMapper;
    }

    @GetMapping
    List<TodoResponse> getAll() {
        return todoService.findAll();
    }

    @PostMapping
    TodoResponse create(@RequestBody TodoCreateRequest todoCreateRequest){
        todoService.createTodo(todoMapper.toEntity(todoCreateRequest));
        return todoMapper.toResponse(todoMapper.toEntity(todoCreateRequest));

    }

    @PutMapping("/{id}")
    void update(@PathVariable Integer id,@RequestBody Todo todo){
        todoService.updateTodo(id,todo);

    }


    @DeleteMapping("/{id}")
    boolean delete(@PathVariable Integer id){
        todoService.deleteTodo(id);
        return false;
    }

}
