package oocl.afs.todolist.service.dto;

public class TodoCreateRequest {
    private String name;

    public TodoCreateRequest() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
