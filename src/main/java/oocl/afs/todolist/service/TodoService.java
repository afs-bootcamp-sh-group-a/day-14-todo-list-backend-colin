package oocl.afs.todolist.service;

import oocl.afs.todolist.advice.ErrorResponse;
import oocl.afs.todolist.advice.NotFoundException;
import oocl.afs.todolist.entity.Todo;
import oocl.afs.todolist.repository.TodoRepository;
import oocl.afs.todolist.service.dto.TodoResponse;
import oocl.afs.todolist.service.mapper.TodoMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TodoService {
    private final TodoRepository todoRepository;

    public TodoService(TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }

    public List<TodoResponse> findAll() {
        return todoRepository.findAll()
                .stream()
                .map(todoEtity -> TodoMapper.toResponse(todoEtity))
                .collect(Collectors.toList());
    }
    public void createTodo(Todo todo){
        todoRepository.save(todo);
    }
    public void updateTodo(Integer id, Todo todo){
        Todo todo1=todoRepository.findById(Long.valueOf(id)).get();
        if (todo1==null) throw new NotFoundException();
        todoRepository.save(todo);
    }
    public void deleteTodo(Integer id){
        Todo todo1=todoRepository.findById(Long.valueOf(id)).get();
        if (todo1==null) throw new NotFoundException();
        todoRepository.deleteById(Long.valueOf(id));
    }



}
