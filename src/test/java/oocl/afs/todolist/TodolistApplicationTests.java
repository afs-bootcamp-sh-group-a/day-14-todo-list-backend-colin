package oocl.afs.todolist;

import oocl.afs.todolist.entity.Todo;
import oocl.afs.todolist.repository.TodoRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
@AutoConfigureMockMvc
@Transactional

@SpringBootTest
class TodolistApplicationTests {

	@Autowired
	MockMvc mockMvc;
	@Autowired
	TodoRepository todoRepository;
	@BeforeEach
	void setUp() {
		todoRepository.deleteAll();
	}


	@Test
	void should_get_all_todos_when_get_given_() throws Exception {

		Todo todo1=new Todo();
		Todo todo2=new Todo();
		todo1.setId(1L);
		todo1.setName("first");
		todo1.setDone(false);
		todo2.setId(2L);
		todo2.setName("first");
		todo2.setDone(false);
		Todo save1 = todoRepository.save(todo1);
		Todo save2 = todoRepository.save(todo2);

		mockMvc.perform(get("/todo"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$[0].id").value(save1.getId()))
				.andExpect(jsonPath("$[1].id").value(save2.getId()));

	}
	@Test
	void should_delete_todo_when_get_given_() throws Exception {

		Todo todo1=new Todo();
		Todo todo2=new Todo();
		todo1.setId(1L);
		todo1.setName("first");
		todo1.setDone(false);
		todo2.setId(2L);
		todo2.setName("first");
		todo2.setDone(false);
		todoRepository.save(todo1);
		Todo saved = todoRepository.save(todo2);

		mockMvc.perform(delete("/todo/" + saved.getId()))
				.andExpect(status().isOk());
		boolean success = todoRepository.findAll().stream()
				.allMatch(todo -> todo.getId().equals(saved.getId()));
		assertFalse(success);


	}
	@Test
	void should_create_todo_when_get_given_() throws Exception {



		String json = "{\"name\":\"test\",\"done\":false}";
		mockMvc.perform(post("/todo").content(json).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
		boolean success = todoRepository.findAll().stream()
				.allMatch(todo -> todo.getName().equals("test"));
		assertTrue(success);



	}

	@Test
	void should_update_todo_name_when_put() throws Exception {
		Todo todo1=new Todo();
		todo1.setId(1L);
		todo1.setName("first");
		todo1.setDone(false);
		Todo saved = todoRepository.save(todo1);
		String json = "{\"id\":\"1\",\"name\":\"test1\",\"done\":false}";
		mockMvc.perform(put("/todo/" + saved.getId()).content(json).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
		boolean success = todoRepository.findAll().stream().filter(todo -> todo.getId()==saved.getId())
				.anyMatch(todo -> todo.getName().equals("test1"));
		assertTrue(success);



	}
	@Test
	void should_update_todo_done_when_put() throws Exception {
		Todo todo1=new Todo();
		todo1.setId(1L);
		todo1.setName("first");
		todo1.setDone(false);
		Todo saved = todoRepository.save(todo1);
		String json = "{\"id\":\"1\",\"name\":\"first\",\"done\":true}";
		mockMvc.perform(put("/todo/" + saved.getId()).content(json).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
		boolean success = todoRepository.findAll().stream().filter(todo -> todo.getId()==saved.getId())
				.anyMatch(todo -> todo.getDone().equals(true));
		assertTrue(success);



	}

}
